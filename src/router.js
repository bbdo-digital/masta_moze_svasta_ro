import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'



Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/cum-participi',
      name: 'cum-participi',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/KakoSudjelovati.vue')
    },
    {
      path: '/votează-pentru-dreptul-la-copilărie',
      name: 'votează-pentru-dreptul-la-copilărie',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    },
    // {
    //   path: '/votează-pentru-dreptul-la-copilărie!',
    //   name: 'votează-pentru-dreptul-la-copilărie!',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/GoodCause.vue')
    // },
    {
      path: '/votează-ți-desenul-preferat',
      name: 'votează-ți-desenul-preferat',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/GlasujZaSvogFavorita.vue')
    },
    {
      path: '/programul-atelierelor-de-desen',
      name: 'programul-atelierelor-de-desen',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RasporedCrtanja.vue')
    },
    {
      path: '/recompense',
      name: 'recompense',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Nagrade.vue')
    },
    {

      path: '/anunțarea-câștigătorilor',
      name: 'anunțarea-câștigătorilor',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/ObjavaDobitnika.vue')
       },
      {
      path: '/norme',
      name: 'norme',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    },
    {
      path: '/norme',
      name: 'norme',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Pravila.vue')

    },
     {
      path: '/despre-colecția-sagoskatt',
      name: 'despre-colecția-sagoskatt',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/SagoskatZivotinje.vue')

    },
    
    { path: "*", 
      name:'404',
    component: () => import(/* webpackChunkName: "about" */ './views/404.vue')
  },
  { path: '*', redirect: '/404' },

  //   { path: '/', component: 'home' },  
  // { path: '*', redirect: '/' }, 
  ]
})
